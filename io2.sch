EESchema Schematic File Version 4
LIBS:kpp2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FPGA_Xilinx_Artix7:XC7A35T-FTG256 U?
U 2 1 5C264F62
P 4050 3900
F 0 "U?" H 4050 926 50  0000 C CNN
F 1 "XC7A35T-FTG256" H 4050 835 50  0000 C CNN
F 2 "" H 4050 3900 50  0001 C CNN
F 3 "" H 4050 3900 50  0000 C CNN
	2    4050 3900
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5C264FE4
P 2650 900
F 0 "#PWR?" H 2650 750 50  0001 C CNN
F 1 "+3.3V" H 2665 1073 50  0000 C CNN
F 2 "" H 2650 900 50  0001 C CNN
F 3 "" H 2650 900 50  0001 C CNN
	1    2650 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1000 5150 950 
Wire Wire Line
	5150 950  5250 950 
Wire Wire Line
	5650 950  5650 1000
Wire Wire Line
	5650 950  5650 900 
Connection ~ 5650 950 
Wire Wire Line
	5550 1000 5550 950 
Connection ~ 5550 950 
Wire Wire Line
	5550 950  5650 950 
Wire Wire Line
	5450 1000 5450 950 
Connection ~ 5450 950 
Wire Wire Line
	5450 950  5550 950 
Wire Wire Line
	5350 1000 5350 950 
Connection ~ 5350 950 
Wire Wire Line
	5350 950  5450 950 
Wire Wire Line
	5250 1000 5250 950 
Connection ~ 5250 950 
Wire Wire Line
	5250 950  5350 950 
Wire Wire Line
	2650 1000 2650 950 
Wire Wire Line
	2650 950  2550 950 
Wire Wire Line
	2450 950  2450 1000
Connection ~ 2650 950 
Wire Wire Line
	2650 950  2650 900 
Wire Wire Line
	2550 1000 2550 950 
Connection ~ 2550 950 
Wire Wire Line
	2550 950  2450 950 
$Comp
L kpp2-rescue:Conn_02x20_oboard4-conn_02x20_counter_clockwise J?
U 1 1 5BFD5A7C
P 6350 5700
F 0 "J?" H 6272 6716 50  0000 R CNN
F 1 "Conn_02x20_oboard4-conn_02x20_counter_clockwise" H 6272 6625 50  0000 R CNN
F 2 "" H 6500 7700 50  0001 C CNN
F 3 "" H 6500 7700 50  0001 C CNN
	1    6350 5700
	-1   0    0    -1  
$EndComp
$Comp
L power:+VDC #PWR?
U 1 1 5C023D22
P 5650 900
F 0 "#PWR?" H 5650 800 50  0001 C CNN
F 1 "+VDC" H 5650 1175 50  0000 C CNN
F 2 "" H 5650 900 50  0001 C CNN
F 3 "" H 5650 900 50  0001 C CNN
	1    5650 900 
	1    0    0    -1  
$EndComp
$EndSCHEMATC
